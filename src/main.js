import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import Vuikit from "vuikit";
import VuikitIcons from "@vuikit/icons";

Vue.use(Vuikit);
Vue.use(VuikitIcons);
Vue.use(Vuetify);

// We first import the Vue library and the App component from App.vue.
import App from "./App.vue";
import Header from "./components/Header.vue";
import Feature from "./components/Feature.vue";
import Team from "./components/Team.vue";
import News from "./components/News.vue";
import Events from "./components/Events.vue";
import Footer from "./components/Footer.vue";

import AOS from "aos";
import "aos/dist/aos.css";

// We set productionTip to false, just to avoid Vue to output a “you’re in development mode” tip in the console.
Vue.config.productionTip = false;

Vue.component("app-header", Header);
Vue.component("app-feature", Feature);
Vue.component("app-team", Team);
Vue.component("app-news", News);
Vue.component("app-events", Events);
Vue.component("app-footer", Footer);

new Vue({
  created() {
    AOS.init();
  },
  render: h => h(App)
}).$mount("#app");

// About render:   https://css-tricks.com/what-does-the-h-stand-for-in-vues-render-method/

// #152437
// #BCBEBD
